﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cong_ty
{
    class Nhan_vien
    {
        public Nhan_vien() { }
        public virtual int MNV { get; set; }
        public virtual string Ho_ten { get; set; }
        public virtual Boolean Gioi_tinh { get; set; }
        public virtual DateTime Ngay_sinh { get; set; }
        public virtual string Dia_chi { get; set; }
        public virtual int MDV { get; set; }
        public virtual int MTD { get; set; }
    }
}
