﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NHibernate;
using NHibernate.Cfg;
using System.Collections;
using NHibernate.Expression;

namespace Cong_ty
{
    public partial class MH_Quan_ly_nhan_vien : Form
    {
        public MH_Quan_ly_nhan_vien()
        {
            InitializeComponent();
        }
        Configuration configuration;
        ISessionFactory factory;
        private void MH_Quan_ly_nhan_vien_Load(object sender, EventArgs e)
        {
            configuration = new Configuration();
            configuration.Configure();
            configuration.AddAssembly(System.Reflection.Assembly.GetExecutingAssembly());
            factory = configuration.BuildSessionFactory();
            //Danh_sach_don_vi = dữ liệu đọc đơn vị từ CSDL
            IList Danh_sach_don_vi, Danh_sach_don_vi_1, Danh_sach_trinh_do;
            using (ISession session = factory.OpenSession())
            {
                Danh_sach_don_vi = session.CreateCriteria(typeof(Cong_ty.Don_vi)).List();
                Danh_sach_don_vi_1 = session.CreateCriteria(typeof(Cong_ty.Don_vi)).List();
                //Danh_sach_trinh_do= dữ liệu đọc trình độ từ CSDL
                Danh_sach_trinh_do = session.CreateCriteria(typeof(Cong_ty.Trinh_do)).List();
            }
            
            //Kết xuất Danh_sach_don_vi vào Danh_sach_chon_don_vi
            XL_Form.Ket_xuat(Danh_sach_don_vi, Danh_sach_chon_don_vi, "MDV", "Ten");
            //Kết xuất Danh_sach_don_vi vào Danh_sach_chon
            XL_Form.Ket_xuat(Danh_sach_don_vi_1, Danh_sach_chon, "MDV", "Ten");
            //Kết xuất Danh_sach_trinh_do vào Danh_sach_chon_trinh_do
            XL_Form.Ket_xuat(Danh_sach_trinh_do, Danh_sach_chon_trinh_do, "MTD", "Ten");
        }

        private void Danh_sach_chon_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Danh_sach = danh sách nhân viên thuộc đơn vị được chọn
            int MDV = int.Parse(Danh_sach_chon.SelectedValue.ToString());
            IList Danh_sach;
            using (ISession session = factory.OpenSession())
            {
                Danh_sach = (IList)session.CreateCriteria(typeof(Cong_ty.Nhan_vien)).Add(Expression.Eq("MDV", MDV)).List();
            }
            //Kết xuất Danh_sach vào Luoi
            Luoi.DataSource = Danh_sach;
            if (Luoi.RowCount == 0)
            {
                return;
            }
            Luoi.Columns["MNV"].Visible = false;
            Luoi.Columns["MDV"].Visible = false;
            Luoi.Columns["MTD"].Visible = false;
            Hien_thi();
        }
        void Hien_thi()
        {
            if (Luoi.Rows.Count == 0)
            {
                return;
            }
            //Nhan_vien = nhân viên đang được chọn
            Nhan_vien nhan_vien = (Nhan_vien) XL_Form.Chon_dong(Luoi);
            //Ho_ten = họ tên tương ứng Nhan_vien
            //Ngay_sinh = ngày sinh tương ứng Nhan_vien
            //Gioi_tinh = giới tính tương ứng Nhan_vien
            //Dia_chi = địa chỉ tương ứng Nhan_vien
            //Don_vi = đơn vị tương ứng Nhan_vien
            //Trinh_do = trình độ tương ứng Nhan_vien
            //Kết xuất Ho_ten vào TH_Ho_ten
            TH_Ho_ten.Text = nhan_vien.Ho_ten;
            //Kết xuất Ngay_sinh vào TH_Ngay_sinh
            TH_Ngay_sinh.Text = nhan_vien.Ngay_sinh.ToShortDateString();
            //Kết xuất Gioi_tinh vào TH_Gioi_tinh
            TH_Gioi_tinh.Checked = nhan_vien.Gioi_tinh;
            //Kết xuất Dia_chi vào TH_Dia_chi
            TH_Dia_chi.Text = nhan_vien.Dia_chi;
            //Kết xuất Don_vi vào TH_Don_vi
            Danh_sach_chon_don_vi.SelectedValue = nhan_vien.MDV;
            //Kết xuất Trinh_do vào TH_Trinh_do
            Danh_sach_chon_trinh_do.SelectedValue = nhan_vien.MTD;
        }
        private void Luoi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Hien_thi();
        }

        private void XL_Them_Click(object sender, EventArgs e)
        {
            //Cua_so = màn hình thêm nhân viên
            MH_Them_nhan_vien Cua_so = new MH_Them_nhan_vien();
            //Mở Cua_so dưới dạng hộp thoại
            Cua_so.ShowDialog();
        }

        private void XL_Sua_Click(object sender, EventArgs e)
        {
            //Kiểm tra tính hợp lệ của dữ liệu nhập
            String Chuoi_loi = XL_Nhan_vien.Kiem_tra(TH_Ho_ten, TH_Ngay_sinh);
            if (Chuoi_loi != "")
            {
                MessageBox.Show(Chuoi_loi);
                return;
            }
            //Nếu hợp lệ
            //---Nhan_vien = nhân viên đang được chọn
            Nhan_vien nhan_vien = (Nhan_vien)XL_Form.Chon_dong(Luoi);
            //---Nhập liệu cho Nhan_vien
            nhan_vien = XL_Nhan_vien.Nhap_lieu(nhan_vien, TH_Ho_ten
            , TH_Gioi_tinh, TH_Ngay_sinh, TH_Dia_chi,
            Danh_sach_chon_don_vi, Danh_sach_chon_trinh_do);
            //---Ghi Nhan_vien
            using (ISession session = factory.OpenSession())
            {
                session.Update(nhan_vien);
                session.Flush();
                MessageBox.Show("Ghi thành công!");
            }
        }

        private void XL_Xoa_Click(object sender, EventArgs e)
        {
            //Xác nhận xóa hay không
            //Nếu xóa
            if (MessageBox.Show("Xóa?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //---Nhan_vien = nhân viên đang được chọn
                Nhan_vien nhan_vien = (Nhan_vien)XL_Form.Chon_dong(Luoi);
                //---Xóa Nhan_vien
                using (ISession session = factory.OpenSession())
                {
                    session.Delete(nhan_vien);
                    session.Flush();
                    MessageBox.Show("Xóa thành công!");
                }
            }
        }
    }
}
