﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Cong_ty
{
    class XL_Nhan_vien
    {
        public static String Kiem_tra(TextBox TH_Ho_ten, TextBox TH_Ngay_sinh)
        {
            String Kq = "";
            //---Họ tên khác trống
            if (TH_Ho_ten.Text.Trim().Length == 0)
            {
                Kq += "Họ tên phải khác rỗng\n";
            }
            //---Ngày sinh khác trống
            if (TH_Ngay_sinh.Text.Trim().Length == 0)
            {
                Kq += "Ngày sinh phải khác rỗng";
            }
            else
            {
                //------Ngày sinh phải là kiểu ngày
                try
                {
                    DateTime Ngay_sinh = DateTime.Parse(TH_Ngay_sinh.Text.ToString());
                }
                catch (Exception)
                {
                    Kq += "Ngày sinh không hợp lệ";
                }
                //---------Ngày sinh trong độ tuổi cho phép (18 đến 50)
            }

            return Kq;
        }
        public static Nhan_vien Nhap_lieu(Nhan_vien nhan_vien, TextBox TH_Ho_ten
            , CheckBox TH_Gioi_tinh, TextBox TH_Ngay_sinh, TextBox TH_Dia_chi,
            ComboBox Danh_sach_chon_don_vi, ComboBox Danh_sach_chon_trinh_do)
        {
            nhan_vien.Ho_ten = TH_Ho_ten.Text;
            nhan_vien.Gioi_tinh = TH_Gioi_tinh.Checked;
            nhan_vien.Ngay_sinh = DateTime.Parse(TH_Ngay_sinh.Text.ToString());
            nhan_vien.Dia_chi = TH_Dia_chi.Text;
            nhan_vien.MDV = int.Parse(Danh_sach_chon_don_vi.SelectedValue.ToString());
            nhan_vien.MTD = int.Parse(Danh_sach_chon_trinh_do.SelectedValue.ToString());
            return nhan_vien;
        }
    }
}
