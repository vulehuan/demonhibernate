﻿namespace Cong_ty
{
    partial class MH_Quan_ly_nhan_vien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Luoi = new System.Windows.Forms.DataGridView();
            this.Danh_sach_chon = new System.Windows.Forms.ListBox();
            this.Danh_sach_chon_trinh_do = new System.Windows.Forms.ComboBox();
            this.Danh_sach_chon_don_vi = new System.Windows.Forms.ComboBox();
            this.TH_Gioi_tinh = new System.Windows.Forms.CheckBox();
            this.XL_Xoa = new System.Windows.Forms.Button();
            this.XL_Sua = new System.Windows.Forms.Button();
            this.XL_Them = new System.Windows.Forms.Button();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.TH_Dia_chi = new System.Windows.Forms.TextBox();
            this.TH_Ngay_sinh = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TH_Ho_ten = new System.Windows.Forms.TextBox();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Luoi)).BeginInit();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Luoi
            // 
            this.Luoi.AllowUserToAddRows = false;
            this.Luoi.AllowUserToDeleteRows = false;
            this.Luoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Luoi.Location = new System.Drawing.Point(4, 243);
            this.Luoi.Name = "Luoi";
            this.Luoi.ReadOnly = true;
            this.Luoi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Luoi.Size = new System.Drawing.Size(458, 144);
            this.Luoi.TabIndex = 6;
            this.Luoi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Luoi_CellClick);
            // 
            // Danh_sach_chon
            // 
            this.Danh_sach_chon.FormattingEnabled = true;
            this.Danh_sach_chon.Location = new System.Drawing.Point(3, 12);
            this.Danh_sach_chon.Name = "Danh_sach_chon";
            this.Danh_sach_chon.Size = new System.Drawing.Size(190, 225);
            this.Danh_sach_chon.TabIndex = 5;
            this.Danh_sach_chon.SelectedIndexChanged += new System.EventHandler(this.Danh_sach_chon_SelectedIndexChanged);
            // 
            // Danh_sach_chon_trinh_do
            // 
            this.Danh_sach_chon_trinh_do.FormattingEnabled = true;
            this.Danh_sach_chon_trinh_do.Location = new System.Drawing.Point(276, 175);
            this.Danh_sach_chon_trinh_do.Name = "Danh_sach_chon_trinh_do";
            this.Danh_sach_chon_trinh_do.Size = new System.Drawing.Size(188, 21);
            this.Danh_sach_chon_trinh_do.TabIndex = 4;
            // 
            // Danh_sach_chon_don_vi
            // 
            this.Danh_sach_chon_don_vi.FormattingEnabled = true;
            this.Danh_sach_chon_don_vi.Location = new System.Drawing.Point(276, 148);
            this.Danh_sach_chon_don_vi.Name = "Danh_sach_chon_don_vi";
            this.Danh_sach_chon_don_vi.Size = new System.Drawing.Size(188, 21);
            this.Danh_sach_chon_don_vi.TabIndex = 4;
            // 
            // TH_Gioi_tinh
            // 
            this.TH_Gioi_tinh.AutoSize = true;
            this.TH_Gioi_tinh.Location = new System.Drawing.Point(276, 42);
            this.TH_Gioi_tinh.Name = "TH_Gioi_tinh";
            this.TH_Gioi_tinh.Size = new System.Drawing.Size(40, 17);
            this.TH_Gioi_tinh.TabIndex = 3;
            this.TH_Gioi_tinh.Text = "Nữ";
            this.TH_Gioi_tinh.UseVisualStyleBackColor = true;
            // 
            // XL_Xoa
            // 
            this.XL_Xoa.Location = new System.Drawing.Point(395, 204);
            this.XL_Xoa.Name = "XL_Xoa";
            this.XL_Xoa.Size = new System.Drawing.Size(67, 33);
            this.XL_Xoa.TabIndex = 2;
            this.XL_Xoa.Text = "Xóa";
            this.XL_Xoa.UseVisualStyleBackColor = true;
            this.XL_Xoa.Click += new System.EventHandler(this.XL_Xoa_Click);
            // 
            // XL_Sua
            // 
            this.XL_Sua.Location = new System.Drawing.Point(322, 204);
            this.XL_Sua.Name = "XL_Sua";
            this.XL_Sua.Size = new System.Drawing.Size(67, 33);
            this.XL_Sua.TabIndex = 2;
            this.XL_Sua.Text = "Sửa";
            this.XL_Sua.UseVisualStyleBackColor = true;
            this.XL_Sua.Click += new System.EventHandler(this.XL_Sua_Click);
            // 
            // XL_Them
            // 
            this.XL_Them.Location = new System.Drawing.Point(249, 204);
            this.XL_Them.Name = "XL_Them";
            this.XL_Them.Size = new System.Drawing.Size(67, 33);
            this.XL_Them.TabIndex = 2;
            this.XL_Them.Text = "Thêm";
            this.XL_Them.UseVisualStyleBackColor = true;
            this.XL_Them.Click += new System.EventHandler(this.XL_Them_Click);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(199, 176);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(47, 13);
            this.Label7.TabIndex = 0;
            this.Label7.Text = "Trình độ";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(199, 150);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 13);
            this.Label6.TabIndex = 0;
            this.Label6.Text = "Đơn vị";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(199, 94);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 13);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "Địa chỉ";
            // 
            // Label1
            // 
            this.Label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(0, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(483, 23);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "QUẢN LÝ NHÂN VIÊN";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TH_Dia_chi
            // 
            this.TH_Dia_chi.Location = new System.Drawing.Point(276, 91);
            this.TH_Dia_chi.Multiline = true;
            this.TH_Dia_chi.Name = "TH_Dia_chi";
            this.TH_Dia_chi.Size = new System.Drawing.Size(188, 50);
            this.TH_Dia_chi.TabIndex = 1;
            // 
            // TH_Ngay_sinh
            // 
            this.TH_Ngay_sinh.Location = new System.Drawing.Point(276, 65);
            this.TH_Ngay_sinh.Name = "TH_Ngay_sinh";
            this.TH_Ngay_sinh.Size = new System.Drawing.Size(188, 20);
            this.TH_Ngay_sinh.TabIndex = 1;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(199, 68);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 13);
            this.Label4.TabIndex = 0;
            this.Label4.Text = "Ngày sinh";
            // 
            // TH_Ho_ten
            // 
            this.TH_Ho_ten.Location = new System.Drawing.Point(276, 13);
            this.TH_Ho_ten.Name = "TH_Ho_ten";
            this.TH_Ho_ten.Size = new System.Drawing.Size(188, 20);
            this.TH_Ho_ten.TabIndex = 1;
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.Luoi);
            this.Panel1.Controls.Add(this.Danh_sach_chon);
            this.Panel1.Controls.Add(this.Danh_sach_chon_trinh_do);
            this.Panel1.Controls.Add(this.Danh_sach_chon_don_vi);
            this.Panel1.Controls.Add(this.TH_Gioi_tinh);
            this.Panel1.Controls.Add(this.XL_Xoa);
            this.Panel1.Controls.Add(this.XL_Sua);
            this.Panel1.Controls.Add(this.XL_Them);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.Label6);
            this.Panel1.Controls.Add(this.TH_Dia_chi);
            this.Panel1.Controls.Add(this.Label5);
            this.Panel1.Controls.Add(this.TH_Ngay_sinh);
            this.Panel1.Controls.Add(this.Label4);
            this.Panel1.Controls.Add(this.TH_Ho_ten);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Location = new System.Drawing.Point(7, 23);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(477, 397);
            this.Panel1.TabIndex = 7;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(199, 16);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(39, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Họ tên";
            // 
            // MH_Quan_ly_nhan_vien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 422);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Panel1);
            this.Name = "MH_Quan_ly_nhan_vien";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MH_Quan_ly_nhan_vien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Luoi)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGridView Luoi;
        internal System.Windows.Forms.ListBox Danh_sach_chon;
        internal System.Windows.Forms.ComboBox Danh_sach_chon_trinh_do;
        internal System.Windows.Forms.ComboBox Danh_sach_chon_don_vi;
        internal System.Windows.Forms.CheckBox TH_Gioi_tinh;
        internal System.Windows.Forms.Button XL_Xoa;
        internal System.Windows.Forms.Button XL_Sua;
        internal System.Windows.Forms.Button XL_Them;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox TH_Dia_chi;
        internal System.Windows.Forms.TextBox TH_Ngay_sinh;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox TH_Ho_ten;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label Label2;
    }
}

