﻿namespace Cong_ty
{
    partial class MH_Them_nhan_vien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Danh_sach_chon_trinh_do = new System.Windows.Forms.ComboBox();
            this.Danh_sach_chon_don_vi = new System.Windows.Forms.ComboBox();
            this.TH_Gioi_tinh = new System.Windows.Forms.CheckBox();
            this.XL_Them = new System.Windows.Forms.Button();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.TH_Dia_chi = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.TH_Ngay_sinh = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TH_Ho_ten = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.Danh_sach_chon_trinh_do);
            this.Panel1.Controls.Add(this.Danh_sach_chon_don_vi);
            this.Panel1.Controls.Add(this.TH_Gioi_tinh);
            this.Panel1.Controls.Add(this.XL_Them);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.Label6);
            this.Panel1.Controls.Add(this.TH_Dia_chi);
            this.Panel1.Controls.Add(this.Label5);
            this.Panel1.Controls.Add(this.TH_Ngay_sinh);
            this.Panel1.Controls.Add(this.Label4);
            this.Panel1.Controls.Add(this.TH_Ho_ten);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Location = new System.Drawing.Point(4, 26);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(287, 235);
            this.Panel1.TabIndex = 9;
            // 
            // Danh_sach_chon_trinh_do
            // 
            this.Danh_sach_chon_trinh_do.FormattingEnabled = true;
            this.Danh_sach_chon_trinh_do.Location = new System.Drawing.Point(85, 165);
            this.Danh_sach_chon_trinh_do.Name = "Danh_sach_chon_trinh_do";
            this.Danh_sach_chon_trinh_do.Size = new System.Drawing.Size(188, 21);
            this.Danh_sach_chon_trinh_do.TabIndex = 5;
            // 
            // Danh_sach_chon_don_vi
            // 
            this.Danh_sach_chon_don_vi.FormattingEnabled = true;
            this.Danh_sach_chon_don_vi.Location = new System.Drawing.Point(85, 138);
            this.Danh_sach_chon_don_vi.Name = "Danh_sach_chon_don_vi";
            this.Danh_sach_chon_don_vi.Size = new System.Drawing.Size(188, 21);
            this.Danh_sach_chon_don_vi.TabIndex = 4;
            // 
            // TH_Gioi_tinh
            // 
            this.TH_Gioi_tinh.AutoSize = true;
            this.TH_Gioi_tinh.Location = new System.Drawing.Point(85, 32);
            this.TH_Gioi_tinh.Name = "TH_Gioi_tinh";
            this.TH_Gioi_tinh.Size = new System.Drawing.Size(40, 17);
            this.TH_Gioi_tinh.TabIndex = 1;
            this.TH_Gioi_tinh.Text = "Nữ";
            this.TH_Gioi_tinh.UseVisualStyleBackColor = true;
            // 
            // XL_Them
            // 
            this.XL_Them.Location = new System.Drawing.Point(85, 192);
            this.XL_Them.Name = "XL_Them";
            this.XL_Them.Size = new System.Drawing.Size(67, 33);
            this.XL_Them.TabIndex = 6;
            this.XL_Them.Text = "Thêm";
            this.XL_Them.UseVisualStyleBackColor = true;
            this.XL_Them.Click += new System.EventHandler(this.XL_Them_Click);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(8, 166);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(47, 13);
            this.Label7.TabIndex = 0;
            this.Label7.Text = "Trình độ";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(8, 140);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 13);
            this.Label6.TabIndex = 0;
            this.Label6.Text = "Đơn vị";
            // 
            // TH_Dia_chi
            // 
            this.TH_Dia_chi.Location = new System.Drawing.Point(85, 81);
            this.TH_Dia_chi.Multiline = true;
            this.TH_Dia_chi.Name = "TH_Dia_chi";
            this.TH_Dia_chi.Size = new System.Drawing.Size(188, 50);
            this.TH_Dia_chi.TabIndex = 3;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(8, 84);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 13);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "Địa chỉ";
            // 
            // TH_Ngay_sinh
            // 
            this.TH_Ngay_sinh.Location = new System.Drawing.Point(85, 55);
            this.TH_Ngay_sinh.Name = "TH_Ngay_sinh";
            this.TH_Ngay_sinh.Size = new System.Drawing.Size(188, 20);
            this.TH_Ngay_sinh.TabIndex = 2;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(8, 58);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 13);
            this.Label4.TabIndex = 0;
            this.Label4.Text = "Ngày sinh";
            // 
            // TH_Ho_ten
            // 
            this.TH_Ho_ten.Location = new System.Drawing.Point(85, 3);
            this.TH_Ho_ten.Name = "TH_Ho_ten";
            this.TH_Ho_ten.Size = new System.Drawing.Size(188, 20);
            this.TH_Ho_ten.TabIndex = 0;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(8, 6);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(39, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Họ tên";
            // 
            // Label1
            // 
            this.Label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(0, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(295, 23);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "THÊM MỚI NHÂN VIÊN";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MH_Them_nhan_vien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 268);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Label1);
            this.Name = "MH_Them_nhan_vien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MH_Them_nhan_vien";
            this.Load += new System.EventHandler(this.MH_Them_nhan_vien_Load);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.ComboBox Danh_sach_chon_trinh_do;
        internal System.Windows.Forms.ComboBox Danh_sach_chon_don_vi;
        internal System.Windows.Forms.CheckBox TH_Gioi_tinh;
        internal System.Windows.Forms.Button XL_Them;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox TH_Dia_chi;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox TH_Ngay_sinh;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox TH_Ho_ten;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}