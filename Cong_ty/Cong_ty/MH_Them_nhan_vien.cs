﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using NHibernate;
using NHibernate.Cfg;
using System.Collections;

namespace Cong_ty
{
    public partial class MH_Them_nhan_vien : Form
    {
        public MH_Them_nhan_vien()
        {
            InitializeComponent();
        }
        Configuration configuration;
        ISessionFactory factory;
        private void MH_Them_nhan_vien_Load(object sender, EventArgs e)
        {
            configuration = new Configuration();
            configuration.Configure();
            configuration.AddAssembly(System.Reflection.Assembly.GetExecutingAssembly());
            factory = configuration.BuildSessionFactory();
            //Danh_sach_don_vi = dữ liệu đọc đơn vị từ CSDL
            IList Danh_sach_don_vi, Danh_sach_trinh_do;
            using (ISession session = factory.OpenSession())
            {
                Danh_sach_don_vi = session.CreateCriteria(typeof(Cong_ty.Don_vi)).List();
                //Danh_sach_trinh_do= dữ liệu đọc trình độ từ CSDL
                Danh_sach_trinh_do = session.CreateCriteria(typeof(Cong_ty.Trinh_do)).List();
            }

            //Kết xuất Danh_sach_don_vi vào Danh_sach_chon_don_vi
            XL_Form.Ket_xuat(Danh_sach_don_vi, Danh_sach_chon_don_vi, "MDV", "Ten");
            //Kết xuất Danh_sach_trinh_do vào Danh_sach_chon_trinh_do
            XL_Form.Ket_xuat(Danh_sach_trinh_do, Danh_sach_chon_trinh_do, "MTD", "Ten");
        }

        private void XL_Them_Click(object sender, EventArgs e)
        {
            //Kiểm tra tính hợp lệ của dữ liệu nhập
            String Chuoi_loi = XL_Nhan_vien.Kiem_tra(TH_Ho_ten, TH_Ngay_sinh);
            if (Chuoi_loi != "")
            {
                MessageBox.Show(Chuoi_loi);
                return;
            }
            //Nếu hợp lệ
            //---Nhan_vien = nhân viên mới
            Nhan_vien nhan_vien = new Nhan_vien();
            //---Nhập liệu cho Nhan_vien
            nhan_vien = XL_Nhan_vien.Nhap_lieu(nhan_vien, TH_Ho_ten
            , TH_Gioi_tinh, TH_Ngay_sinh, TH_Dia_chi,
            Danh_sach_chon_don_vi, Danh_sach_chon_trinh_do);
            //---Ghi Nhan_vien
            using (ISession session = factory.OpenSession())
            {
                session.Save(nhan_vien);
                session.Flush();
                MessageBox.Show("Ghi thành công!");
            }
        }
    }
}
