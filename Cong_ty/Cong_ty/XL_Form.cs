﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace Cong_ty
{
    class XL_Form
    {
        public static object Chon_dong(DataGridView Luoi)
        {
            IList Bang = (IList)Luoi.DataSource;
            int Chi_so = Luoi.SelectedRows[0].Index;
            return Bang[Chi_so];
        }
        public static void Ket_xuat(IList Danh_sach, ComboBox Danh_sach_chon, string ValueMember, string DisplayMember)
        {
            Danh_sach_chon.DisplayMember = DisplayMember;
            Danh_sach_chon.ValueMember = ValueMember;
            Danh_sach_chon.DataSource = Danh_sach;
        }
        public static void Ket_xuat(IList Danh_sach, ListBox Danh_sach_chon, string ValueMember, string DisplayMember)
        {
            Danh_sach_chon.DisplayMember = DisplayMember;
            Danh_sach_chon.ValueMember = ValueMember;
            Danh_sach_chon.DataSource = Danh_sach;
        }
    }
}
